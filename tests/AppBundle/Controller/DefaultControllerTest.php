<?php

namespace AppBundle\Tests\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase {
 
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
      
         $form = $crawler->selectButton('submit')->form();

        // set some values
        $form['form[Long-Url]'] = 'http://techcrunch.com';

        // submit the form
        $client->submit($form);
    }
    
    
}

