natae developed symfony 3 last version
=====

*Installation

1/ Run 'composer update', to instal vendor files

2/ Run 'bin/console doctrine:schema:update --force' to create database.

3/ to run project use command 'bin/console server:run' project available to view http://127.0.0.1:8000

*Project 

1/ Currently assumed the main url for our site will be a domain we've bought similar to bit.ly , with a short domain name and extension.  Currently will be working off the local 127.0.0.1:8000 server

2/ This url will have a short extension, for each unique link.

3/ Enter the domain you want to shorten in the form.  It will generate a short URL.

*TODO 

1/ need to check the domain is valid format 

2/ further unit tests need to be create to test the url created works