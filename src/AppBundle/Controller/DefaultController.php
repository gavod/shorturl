<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UrlShortener;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DefaultController extends Controller {
  
    public $url;
    public $rand_url;
    public $short_url;
    
      /**
     * @Route("/", name="")
     */
    public function indexAction()   {
                
        $form = $this->createFormBuilder()
            
        ->setAction($this->generateUrl('url'))
        ->setMethod('POST')
        ->add('Long-Url', TextType::class)
        ->getForm(); 
                 
        return $this->render('AppBundle:Default:index.html.twig',
        array('form' => $form->createView())
        );
              
     }
     
     /**
      * @Route ("/url", name="url")
      */
     public function urlAction(Request $request) { 
              
        $data = $request->request->all();
    
        $this->url = $data['form']['Long-Url']; 
                
       do { 
        // create random 7 character string 
             
        $this->rand_url = substr(str_shuffle(MD5(microtime())), 0, 7);
        
        $this->short_url = $this->get('doctrine')->getManager()->getRepository('AppBundle:UrlShortener')->findOneBy(['shortUrl' => $this->url]);
                
                // if not already used, enter record then break 

                if($this->short_url==null) {  
                $toenter = new UrlShortener();
                $enterit = $toenter->setShortUrl($this->rand_url);
                $enterit = $toenter->setLongUrl($this->url);
                $this->get('doctrine')->getManager()->persist($enterit);
                $this->get('doctrine')->getManager()->flush();
                
                // need to detect ip for live sites 
                $this->base = 'http://127.0.0.1:8000';
                
                return new Response("<html><body>Your shortened URL is <a href=$this->base/$this->rand_url>$this->base/$this->rand_url</a>");
               
                break;
                
                }       
               
      } while (0);
        
     }
     
     /**
     * @Route ("/{smallurl}",name="smallurl")
     */
     
      public function findurlAction($smallurl) {
         
     $long_url = $this->get('doctrine')->getManager()->getRepository('AppBundle:UrlShortener')->findOneBy(['shortUrl' => $smallurl]);
     
     if($long_url ==! null) {
      
     return $this->redirect($long_url->getLongUrl());  
   
     } else { 
          
     return new Response('<html><body>url not found</body></html>');  
     
       }
      
    }
    
}
