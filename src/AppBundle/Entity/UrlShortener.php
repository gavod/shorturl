<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * UrlShortener
 */
class UrlShortener
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Assert\Ip
     */
    
    private $longUrl;

  
    /**
     * @var string
     */
    private $shortUrl;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set longUrl
     *
     * @param string $longUrl
     *
     * @return UrlShortener
     */
    public function setLongUrl($longUrl)
    {
        $this->longUrl = $longUrl;

        return $this;
    }

    /**
     * Get longUrl
     *
     * @return string
     */
    public function getLongUrl()
    {
        return $this->longUrl;
    }



    /**
     * Set shortUrl
     *
     * @param string $shortUrl
     *
     * @return UrlShortener
     */
    public function setShortUrl($shortUrl)
    {
        $this->shortUrl = $shortUrl;

        return $this;
    }

    /**
     * Get shortUrl
     *
     * @return string
     */
    public function getShortUrl()
    {
        return $this->shortUrl;
    }
}

